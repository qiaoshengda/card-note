export const statistics = (() => {

    return {
        /**
         * 时间埋点
         * @param event 事件
         * @param data 参数
         */
        track(event: string, data?: string): void {
            if (import.meta.env.DEV) {
                console.log('开发环境：', event);
                return;
            }
            try {
                umami.track(event, data ? ({data: data}) : undefined);
            } catch (e) {
                console.error(e);
            }
        }
    }
})();
